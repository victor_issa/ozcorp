package br.com.ozcorp;

public enum TipoSanguineo {
	A_POSITIVO ("A-positivo"),
	A_NEGATIVO ("A-negativo"),
	B_POSITIVO ("B-positivo"),
	B_NEGATIVO ("B-negativo"),
	AB_POSITIVO ("AB-positivo"),
	AB_NEGATIVO ("AB-negativo"),
	O_POSITIVO ("O-positivo"),
	O_NEGATIVO ("O-negativo");
	
	public String nome;
	
	TipoSanguineo(String nome) {
		this.nome = nome;
	}
}
