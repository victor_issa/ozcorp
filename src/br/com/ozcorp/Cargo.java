package br.com.ozcorp;

/**
 * @author Victor Eiji Issa
 *
 */

public class Cargo {
	private String titulo;
	private double SalarioBase;

	// � preciso criar um construtor
	public Cargo(String titulo, double salarioBase) {
		this.titulo = titulo;
		SalarioBase = salarioBase;
	}

	// � preciso criar os getters e setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return SalarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		SalarioBase = salarioBase;
	}
}
